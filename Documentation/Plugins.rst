Plugins package
===============

Subpackages
-----------

.. toctree::

    Plugins.Bucky
    Plugins.CT
    Plugins.Dicom
    Plugins.Fluoro
    Plugins.MRI
    Plugins.Mammo
    Plugins.Monitors
    Plugins.NM
    Plugins.OCRtool
    Plugins.PET
    Plugins.US

Module contents
---------------

.. automodule:: Plugins
    :members:
    :undoc-members:
    :show-inheritance:
